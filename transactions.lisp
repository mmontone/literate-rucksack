;;-- @ignore

(in-package :rucksack)

;;-- @end ignore

#|

@section Transactions

@subsection Atomicity, isolation, durability

Changes to data often need to occur in so called 'atomic' groups.
This means that either *all* of the changes should be executed, or
none of them should be executed.  Such an all-or-nothing group of
changes is usually called a transaction.  When an error occurs in the
middle of a transaction, all changes that were executed by the
transaction must be undone: this is called a 'rollback'.  When the
transaction has finished without any errors, all changes must be saved
to disk: this is called a 'commit'.

Apart from atomicity ('all-or-nothingness') and durability (saving all
changes) another important requirement for transactions is that they
may not interfere with each other.  As long as a transaction hasn't
committed its changes, it must run as if it were the only transaction
using the database.  In other words, it must appear to be 'isolated'
from all other transactions.

Bank accounts are a popular example when discussing transactions.
Let's look at the following function to transfer money from one
account to another.

\begin{minted}[fontsize=\footnotesize]{common-lisp}

(defun transfer-money (amount account-a account-b)
  "Transfer AMOUNT from ACCOUNT-A to ACCOUNT-B."
  (with-transaction ()
    ;; Check that there's enough money in the account.
    (unless (plusp (- (slot-value account-a 'balance) amount))
      (error "Not enough money."))
    ;; Subtract the amount from account A.
    (decf (slot-value account-a 'balance) amount)
    ;; Add the same amount to account B.
    (incf (slot-value account-b 'balance) amount)))

\end{minted}

Now suppose two transactions T1 and T2 are trying to transfer 100 EURO
from an account that contains 150 EURO.  If the bank is not careful,
we could get the following sequence of events:

1. T1: check that there's enough money -> OK
2. T2: check that there's enough money -> OK
3. T1: subtract 100 EURO from the account -> now it contains 50 EURO.
4. T2: subtract 100 EURO from the account -> OOPS.

This is one kind of error that can happen when two transactions are
not isolated from each other.

@subsection Possible solutions

To guarantee that transactions run in isolation, we could:

 - Let the application programmer worry about it and write locking
   code where necessary.

   I haven't chosen this solution.  Keeping transactions isolated is
   a difficult problem where it's easy to make mistakes that are hard
   to detect.

 - Use some kind of automatic locking.

   The best known automatic locking strategy is called "two phase
   locking".  This basically means that a transaction acquires a
   lock for each object that it wants to change, and then doesn't
   release the lock until the entire transaction has finished.

   I haven't chosen this solution either.  One potential problem with
   two phase locking is that long-running transactions (for example
   a transaction that looks at all bank accounts to make a monthly
   report) can easily lock up all other transactions.  Now there may
   be solutions for this too, but I felt these were getting too
   complicated.


@subsection Rucksack's solution

So what does Rucksack do?  It uses so called 'optimistic concurrency
control' combined with 'multiple object versions'.

'Optimistic concurrency control' means that Rucksack doesn't use locks
to keep its transactions isolated from each other.  Instead it just
rolls back a transaction when it is trying to change data that it
shouldn't change, basically telling it to 'try again later'.

Rolling back and retrying a transaction can be rather expensive;
that's why this strategy is called 'optimistic': it assumes that this
kind of transaction conflict happens rarely.

Rucksack tries to make transaction conflicts more rare by using
multiple object versions.  With multiple object versions, each
transaction that modifies an object gets its own copy of that object.

This means that an older transaction can stay in its own consistent
little world and happily keep reading older versions of objects that
are already being changed by younger transactions.  So one transaction
can have its cake while another transaction is eating it ;-)

That doesn't mean that both transactions can be eating the cake at the
same time, of course.  In that case, Rucksack will abort the second
transaction that tries to eat the cake and give it an opportunity to
retry later.  If there's any cake left, of course.

@subsection Safety net

The fact that Rucksack uses optimistic concurrency control does not
mean that you can't use manual or automatic locking on a higher level.
It would be possible to use manual locking and treat Rucksack's
transaction conflict detection mechanism as a sort of safety net, for
example.


@subsection Detecting conflicts

So how are transaction conflicts detected?

Each transaction has a unique ID.  For each version of each object, we
register the ID of the transaction that created/modified the object.
The ID also functions as a relative timestamp: transaction A is older
than transaction B if its ID is less than the ID of transaction B.

Each object creation/modification always happens in the context of a
transaction.  A transaction conflict occurs when an old transaction
tries to modify an object that was modified by a younger transaction.

(Actually, there only needs to be a serialization conflict when an old
transaction tries to modify a slot that was modified by a younger
transaction.  But Rucksack detects conflicts at the object level, not
the slot level.)


@subsection Transactions and the cache

Before we had transactions, the cache was relatively simple: it kept a
set of 'clean' objects: objects that had been read from disk but had
not been changed and a set of 'dirty' objects: objects that had been
modified and needed to be written back to disk.

With multiversion transactions, this design needs to change.  

Here's a simplified version of Rucksack's class definition for CACHE:

\begin{minted}[fontsize=\footnotesize]{common-lisp}

(defclass standard-cache (cache)
  ((heap :initarg :heap :reader heap)
   (schema-table :initarg :schema-table :reader schema-table)
   (commit-file :reader commit-file)
   ;; Clean objects
   (objects :initarg :objects :reader objects)
   (highest-transaction-id :initarg :highest-transaction-id
                           :initform 0
                           :accessor highest-transaction-id)
   (transactions :initform (make-hash-table)
                 :reader transactions
                 :documentation "A mapping from transaction ids to
transactions.  Contains only open transactions, i.e. transactions that
haven't been rolled back or committed.")))

\end{minted}

And here's the definition of TRANSACTION:

\begin{minted}[fontsize=\footnotesize]{common-lisp}

(defclass standard-transaction (transaction)
  ((id :initarg :id :reader transaction-id)
   (dirty-objects :initform (make-hash-table)
                  :reader dirty-objects
                  :documentation "A hash-table (from id to object)
containing all objects of which the slot changes have not been written
to file yet.")))

\end{minted}


@subsection Getting objects from the cache

So what happens when an object must be retrieved from the cache?

With multiple object versions, a transaction is only allowed to see
the version that it has modified itself.  Or, if it hasn't modified
the object, the version that was modified by the youngest transaction
that's older than itself.

For example: if the current transaction is \#3, and the object has been
modified by transactions \#1 and \#5.  Then the 'compatible' object
version is the one that was modified by \#1.

CODE FRAGMENT

\begin{minted}[fontsize=\footnotesize]{common-lisp}

(defmethod cache-get-object (object-id (cache standard-cache))
  (let ((transaction (current-transaction)))
    (or ;; Unmodified, already loaded and compatible with the
        ;; current transaction?  Fine, let's use it.
        (let ((object (gethash object-id (objects cache))))
          (and object
               (<= (transaction-id object) (transaction-id transaction))
               object))
        ;; Modified by an open transaction?  Try to find the
        ;; 'compatible' version.
        (find-object-version object-id transaction cache)
        ;; Not in memory at all? Then load the compatible version
        ;; from disk.
        (multiple-value-bind (object most-recent-p)
            (load-object object-id transaction cache)
          (when most-recent-p
            ;; Add to in-memory cache if the loaded object is
            ;; the most recent version of the object.
            (when (cache-full-p cache)
              (make-room-in-cache cache))
            (setf (gethash object-id (objects cache)) object))
          object))))

\end{minted}


@subsection Rolling back a transaction

When a transaction is rolled back, all side effects of the transaction
must be undone.  There must be no noticeable difference between an
rolled back transaction and a transaction that hasn't even started
yet.

Rolling back is only possible for a transaction that hasn't been
committed yet (and is not currently being committed).  This makes
rolling back easy: the transaction can basically just clear its dirty
objects table to ensure that no changes to those objects will be
written to disk.

So here's some more code:

\begin{minted}[fontsize=\footnotesize]{common-lisp}

(defmethod transaction-rollback-1 ((transaction standard-transaction)
                                   (cache standard-cache)
                                   (rucksack standard-rucksack))
  (clrhash (dirty-objects transaction))
  (queue-clear (dirty-queue transaction))
  (close-transaction cache transaction))

\end{minted}

@subsection API

@emph{User API:}
transaction-start
transaction-commit
transaction-rollback
with-transaction
current-transaction

@emph{Internal API:}
transaction standard-transaction
transaction-start-1

@subsection Implementation
  
|#

(defgeneric transaction-start-1 (cache rucksack &key &allow-other-keys)
  (:documentation "Creates and returns a new transaction."))

(defgeneric transaction-commit-1 (transaction cache rucksack)
  (:documentation "Save all modified objects to disk."))

(defgeneric transaction-rollback-1 (transaction cache rucksack))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Transactions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass transaction ()
  ())

(defclass standard-transaction (transaction)
  ((id :initarg :id :reader transaction-id)
   ;; Dirty objects
   (dirty-objects :initarg :dirty-objects
                  :initform (make-hash-table)
                  :reader dirty-objects
                  :documentation "A hash-table \(from id to object)
containing all objects of which the slot changes have not been written
to disk yet.")
   (dirty-queue :initarg :dirty-queue
                :initform (make-instance 'queue)
                :reader dirty-queue
                :documentation "A queue with the ids of all objects
that have been created or modified since the last commit.  The queue
is in least-recently-dirtied-first order.  During a commit, the
objects are written to disk in the same order \(this is necessary to
guarantee that the garbage collector never sees an id of an object
that doesn't exist on disk yet.")))

(defmethod print-object ((transaction transaction) stream)
  (print-unreadable-object (transaction stream :type t :identity nil)
    (format stream "#~D with ~D dirty object~:P"
            (transaction-id transaction)
            (hash-table-count (dirty-objects transaction)))))


(defun current-transaction ()
  *transaction*)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Modifying objects and checking for conflicts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric transaction-changed-object (transaction object-id)
  (:documentation
   "If the given transaction has modified the object with the given
object id, this function returns the modified object.  Otherwise it
returns nil."))

(defgeneric transaction-older-p (a b)
  (:documentation
   "Returns true iff transaction A is older than transaction B."))

(defgeneric find-conflicting-transaction (object-id cache transaction)
  (:documentation
   "Tries to find an open transaction that has modified the object
with the given object-id and is older than the given transaction.
Returns this conflicting transaction, if there is one.  Otherwise it
returns nil."))

(defmethod transaction-nr-dirty-objects ((transaction standard-transaction))
  (hash-table-count (dirty-objects transaction)))

(defmethod transaction-touch-object ((transaction standard-transaction)
                                     object
                                     object-id)
  (setf (gethash object-id (dirty-objects transaction)) object)
  (queue-add (dirty-queue transaction) object-id))


(defmethod transaction-changed-object ((transaction standard-transaction)
                                       object-id)
  (gethash object-id (dirty-objects transaction)))


(defmethod find-conflicting-transaction
           (object-id
            (cache standard-cache)
            (current-transaction standard-transaction))
  ;; EFFICIENCY: We need to consider all transactions, because the
  ;; transactions are in a hash-table.  If we use a container that's
  ;; ordered by creation time (like a btree), we only need to consider
  ;; transactions that are younger than the given transaction.
  (loop for transaction being the hash-value of (transactions cache)
        thereis (and (not (eql transaction current-transaction))
                     (transaction-older-p transaction current-transaction)
                     (transaction-changed-object transaction object-id)
                     transaction)))


(defmethod transaction-older-p ((a standard-transaction)
                                (b standard-transaction))
  (< (transaction-id a) (transaction-id b)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Starting a new transaction
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun transaction-start (&rest args
                          &key (rucksack (current-rucksack))
                          &allow-other-keys)
  (apply #'transaction-start-1 (rucksack-cache rucksack) rucksack args))


(defmethod transaction-start-1 ((cache standard-cache)
                                (rucksack standard-rucksack)
                                &key &allow-other-keys)
  ;; Create new transaction.
  (let* ((id (incf (highest-transaction-id rucksack)))
         (transaction (make-instance 'standard-transaction :id id)))
    ;; Add to open transactions.
    (open-transaction cache transaction)
    ;; And return the new transaction.
    transaction))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Rucksacks with serial transactions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass serial-transaction-rucksack (standard-rucksack)
  ((transaction-lock :initform (make-lock :name "Rucksack transaction lock")
                     :reader rucksack-transaction-lock))
  (:documentation
   "A serial transaction rucksack allows only one active transaction
at a time."))

(defmethod transaction-start-1 :before ((cache standard-cache)
                                        (rucksack serial-transaction-rucksack)
                                        &key &allow-other-keys)
  (process-lock (rucksack-transaction-lock rucksack)))

(defmethod transaction-commit-1 :after ((transaction standard-transaction)
                                        (cache standard-cache)
                                        (rucksack serial-transaction-rucksack))
  (process-unlock (rucksack-transaction-lock rucksack)))

(defmethod transaction-rollback-1 :after ((transaction standard-transaction)
                                          (cache standard-cache)
                                          (rucksack serial-transaction-rucksack))
  (process-unlock (rucksack-transaction-lock rucksack)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Committing a transaction
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; use without-rucksack-gcing to locally set
;;; *collect-garbage-on-commit* to nil in order to supress rucksack
;;; garbage collection on commit
(defmacro without-rucksack-gcing (&body body)
  `(let ((*collect-garbage-on-commit* nil))
     ,@body))

(defun transaction-commit (transaction &key (rucksack (current-rucksack)))
  "Call transaction-commit-1 to do the real work."
  (transaction-commit-1 transaction (rucksack-cache rucksack) rucksack))

(defmethod transaction-commit-1 ((transaction standard-transaction)
                                 (cache standard-cache)
                                 (rucksack standard-rucksack))
  ;; Save all dirty objects to disk.
  (if (zerop (transaction-nr-dirty-objects transaction))
      (close-transaction cache transaction)
    (progn
      ;; 1. Create the commit file
      (create-commit-file transaction cache)
      ;; 2. Commit all dirty objects.
      ;; Q: What if this is interleaved with other commits?
      (let ((queue (dirty-queue transaction))
            (table (dirty-objects transaction))
            (heap (heap cache))
            nr-allocated-octets)
        (with-allocation-counter (heap)
          (loop until (queue-empty-p queue)
                do (let* ((id (queue-remove queue))
                          (object (gethash id table)))
                     (when object
                       ;; If it's not in the dirty-objects table anymore, the
                       ;; object was already saved during this transaction-commit.
                       ;; That's possible, because the queue can contain duplicates.
                       (save-dirty-object object cache transaction id)
                       ;; Remove from hash-table too.
                       (remhash id table))))
          (setq nr-allocated-octets (nr-allocated-octets heap)))
        ;; Check for consistency between hash table and queue.
        (unless (zerop (hash-table-count table))
          (internal-rucksack-error
           "Mismatch between dirty hash-table and queue while committing ~S:
~D objects left in hash-table."
           transaction
           (hash-table-count table)))
        ;; 3. Remove transaction from the cache's open transactions.
        (close-transaction cache transaction)
        ;; 4. Delete the commit file to indicate that everything went fine
        ;; and we don't need to recover from this commit.
        (delete-commit-file transaction cache)
        ;; 5. Let the garbage collector do an amount of work proportional
        ;; to the number of octets that were allocated during the commit.
        (when *collect-garbage-on-commit*
          (collect-some-garbage heap
                                (gc-work-for-size heap nr-allocated-octets)))
        ;; 6. Make sure that all changes are actually on disk before
        ;; we continue.
        (finish-all-output rucksack)))))

(defmethod finish-all-output ((rucksack standard-rucksack))
  (let ((cache (rucksack-cache rucksack)))
    (finish-heap-output (heap cache))
    (finish-heap-output (object-table (heap cache)))
    ;; NOTE: I'm not totally sure that saving the schema table for
    ;; each transaction commit is necessary, but it probably is.  So
    ;; let's play safe for now.  We definitely need to save the roots,
    ;; because the highest transaction-id is part of the roots file.
    (save-roots rucksack)
    (save-schema-table-if-necessary (schema-table cache))))

                                        
;;
;; Commit file
;;

(defun create-commit-file (transaction cache)
  "Write object ids of all dirty objects to the commit file, so
recovery can do its job if this transaction never completes."
  (with-open-file (stream (commit-filename cache)
                          :direction :output
                          :if-exists :supersede
                          :if-does-not-exist :create
                          :element-type '(unsigned-byte 8))
    (serialize (transaction-id transaction) stream)
    (serialize (hash-table-count (dirty-objects transaction)) stream)
    (loop for object-id being the hash-key of (dirty-objects transaction)
          do (serialize object-id stream))))

(defun delete-commit-file (transaction cache)
  (declare (ignore transaction))
  (delete-file (commit-filename cache)))

(defun load-commit-file (cache)
  "Returns two values: a transaction id and a list of object ids
(of objects that may be partially committed)."
  (with-open-file (stream (commit-filename cache)
                          :direction :output
                          :if-exists :supersede
                          :if-does-not-exist :create
                          :element-type '(unsigned-byte 8))
    (let* ((transaction-id (deserialize stream))
           (nr-objects (deserialize stream))
           (objects (loop repeat nr-objects
                          collect (deserialize stream))))
      (values transaction-id objects))))

;;
;; Saving objects
;;

(defmethod save-dirty-object (object
                              (cache standard-cache)
                              (transaction standard-transaction)
                              object-id &key schema)
  (let* ((transaction-id (transaction-id transaction))
         (heap (heap cache))
         (object-table (object-table heap))
         (version-list
          ;; If the object-table entry is not marked :reserved, there
          ;; is an object version list.  Get the start of that list.
          (and (not (eql :reserved (object-info object-table object-id)))
               (object-heap-position object-table object-id))))
    (multiple-value-bind (younger-version older-version)
        ;; Determine the correct position in the version list.
        (version-list-position transaction-id object-id version-list heap)
      ;; Write the object to a fresh block on the heap.
      (let ((block (save-object object object-id cache 
                                transaction-id older-version
                                :schema schema)))
        ;; Hook the block into the version list.
        (if younger-version
            ;; Let younger version point to this version.
            (setf (object-version-list younger-version heap) block)
          ;; There is no younger version, so this version becomes
          ;; the start of the version list.
          (setf (object-heap-position object-table object-id)
                block)))))
  object-id)

(defun version-list-position (current-transaction-id obj-id version-list heap)
  "Returns the correct position for a transaction-id in a version-list.
To be more precise, it returns:
  1. the block of the object version with the oldest transaction that's
younger than the given transaction-id (nil if there is no such version).
  2. the block of the first object version in the version list that has
a transaction id older than the given transaction-id (nil if there is no
such version).
  VERSION-LIST is either nil or the heap position of the first object
version in the version list."
  (and version-list
       (let ((younger nil)
             (block version-list))
         (loop
          (let ((buffer (load-block heap block :skip-header t)))
            (multiple-value-bind (id nr-slots schema transaction-id previous)
                (load-object-fields buffer obj-id)
              ;; DO: Don't load id, nr-slots, schema at all!
              (declare (ignore id nr-slots schema)) 
              (cond ((< transaction-id current-transaction-id)
                     ;; The version we're examining is older than the
                     ;; current-transaction-id, so we found the right
                     ;; place for the current version.
                     (return-from version-list-position
                       (values younger block)))
                    ((null previous)
                     ;; There is no version that's older than the current
                     ;; transaction.  This can happen, because transaction
                     ;; commits do not necessarily happen in transaction
                     ;; creation order.
                     (return-from version-list-position
                       (values younger nil)))
                    (t
                     ;; Keep trying older versions.
                     (setq younger block
                           block previous)))))))))

(defun (setf object-version-list) (old-block young-block heap)
  "Let the (previous pointer of the) object in YOUNG-BLOCK point to
OLD-BLOCK."
  (let ((stream (heap-stream heap)))
    (file-position stream (+ young-block (block-header-size heap)))
    (serialize-previous-version-pointer old-block stream))
  old-block)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Rolling back
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun transaction-rollback (transaction &key (rucksack (current-rucksack)))
  (transaction-rollback-1 transaction
                          (rucksack-cache rucksack)
                          rucksack))

(defmethod transaction-rollback-1 ((transaction standard-transaction)
                                   (cache standard-cache)
                                   (rucksack standard-rucksack))
  (clrhash (dirty-objects transaction))
  (queue-clear (dirty-queue transaction))
  (close-transaction cache transaction))


 


        

