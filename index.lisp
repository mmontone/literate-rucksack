;;-- @ignore

(in-package :rucksack)

;;-- @end ignore

#|

@section Indexing

@subsection Introduction

So... We know how to save persistent objects and load them back, we
can cache the load operation for better performance, we can reclaim
unused disk space when necessary, we can use transactions to keep
changes isolated from changes in other processes, and we even stand a
fair chance of recovering from failure.

What more could we need?  Ah yes...  Sometimes we want to find some
objects we're interested in without traversing the whole database.  In
other words, we need a persistent indexing mechanism.

Unlike many other database or persistence libraries, Rucksack builds
indexes on top of persistent objects, not the other way round.  Maybe
this costs us a bit in performance because we can't use any low level
tricks for the indexing.  But in return for that we get a flexible and
easily extendible indexing mechanism.

Here's how it works:


@subsection More MOP magic

We already needed some MOP magic to hook into SLOT-VALUE-USING-CLASS
and INITIALIZE-INSTANCE for persistent objects.  Now we need some more
magic to add our own slot and class options for specifying indexes.

This means we can specify something like the following:

\begin{minted}[fontsize=\footnotesize]{common-lisp}

(defclass person ()
  ((name :index (btree :key< string< :key= string=))
   (year-of-birth :index (btree :key< < :key= =))
   (age :persistence nil))
  (:indexed t)
  (:metaclass persistent-class))

\end{minted}

You see an :INDEX slot option being used for the NAME and
YEAR-OF-BIRTH slots, a :PERSISTENCE slot option for the AGE slot and
an :INDEX class option for the whole PERSON class.

Let's look at the class option first: by specifying :INDEXED T, we
specify that the object IDs of all instances of this class (or one of
its subclasses) will be added to an index.  Then the generic function
RUCKSACK-MAP-INSTANCES can be used to iterate over all instances of
this class (or one of its subclasses, unless those subclasses have an
:INDEXED NIL class option).

For slot options the situation is similar, except that you can use so
called index specs to specify explicitly what kind of indexing should
be used.  An index spec has a simple structure: it is either a symbol
or a list.  If it's a symbol, it's the name of an index class.  If
it's a list, the car of the list is the name of an index class, and
the cdr contains a plist of initargs for the index.

The index class can be an arbitrary class (including classes that you
define yourself) as long as it's persistent and follows a simple
indexing protocol.  At the moment, Rucksack has only one index class:
a no-nonsense, catch-as-catch-can implementation of btrees, written on
top of persistent conses and persistent vectors.


@subsection Indexes and garbage collection

All class and slot indexes are automatically added to Rucksack's set
of root objects, so indexed objects won't be removed by the garbage
collector. All non-indexed objects are NOT part of the root set, so
they will be garbage collected if they're unreachable from one of the
indexes.


@subsection Queries (if time left)

I think it should be relatively easy to build a simple query language
on top of RUCKSACK-MAP-INSTANCES and RUCKSACK-MAP-SLOT-VALUES.  This
should make it possible to generate relatively efficient code for
queries like:

\begin{minted}[fontsize=\footnotesize]{common-lisp}

(select 'event
        (lambda (event) ...)
        :where '(and (string= name "ECLM")
                     (= year 2006))
        :order-by 'name)

\end{minted}

I think there's quite a bit of literature on query optimization and
Lisp's fantastic code transformation and run-time compilation features
may add some interesting new possibilites to the standard repertoire.
But that's still open territory.  At least for me ;-)


@subsection Multi-dimensional indexes (if time left)

Another interesting Rucksack extension that I haven't written yet
would be to define multi-dimensional indexes (i.e. indexes that look
at the value of more than one slot).

For example:

\begin{minted}[fontsize=\footnotesize]{common-lisp}

(defclass point ()
  ((x :type number)
   (y :type number))
  (;; Don't use standard indexing (by object id), because we're not
   ;; interested in EQL-ness for points.
   :indexed nil 
   ;; Index points by their coordinates instead.
   :index ((x y) (2d-index :key< (< <) :key= (= =))))
  (:metaclass persistent-class))

\end{minted}

I'm just making something up here.  Maybe another syntax would be
needed for multi-dimensional indexing.  My main point here is that it
is relatively easy to create such extensions yourself and integrate
them with the rest of Rucksack without having to resort to low-level
hacks.


@subsection Implementation

@subsubsection Class and slot indexing
@insert class-and-slot-indexing-api
@include p-btrees.lisp

|#

(defgeneric map-index (index function
                       &key equal min max include-min include-max order)
  (:documentation "Calls FUNCTION for all key/value pairs in the btree
where key is in the specified interval. FUNCTION must be a binary
function; the first argument is the index key, the second argument is
the index value (or list of values, for indexes with non-unique keys).

If EQUAL is specified, the other arguments are ignored; the function
will be called once (if there is a key with the same value as EQUAL)
or not at all (if there is no such key).

MIN, MAX, INCLUDE-MIN and INCLUDE-MAX specify the interval.  The
interval is left-open if MIN is nil, right-open if MAX is nil.  The
interval is inclusive on the left if INCLUDE-MIN is true (and
exclusive on the left otherwise).  The interval is inclusive on the
right if INCLUDE-MAX is true (and exclusive on the right otherwise).

ORDER is either :ASCENDING (default) or :DESCENDING."))

(defgeneric index-insert (index key value &key if-exists)
  (:documentation
 "Insert a key/value pair into an index.  IF-EXISTS can be either
:OVERWRITE (default) or :ERROR."))

(defgeneric index-delete (index key value &key if-does-not-exist)
  (:documentation
 "Remove a key/value pair from an index.  IF-DOES-NOT-EXIST can be
either :IGNORE (default) or :ERROR."))

;; make-index (index-spec unique-keys-p) [Function]

;; index-spec-equal (index-spec-1 index-spec-2) [Function]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Index class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass index ()
  ((spec :initarg :spec :reader index-spec)
   (unique-keys-p :initarg :unique-keys-p :reader index-unique-keys-p)
   (data :initarg :data :reader index-data
         :documentation "The actual index data structure (e.g. a btree)."))
  (:metaclass persistent-class)
  (:index nil))

(defmethod print-object ((index index) stream)
  (print-unreadable-object (index stream :type t :identity t)
    (format stream "~S with ~:[non-unique~;unique~] keys"
            (index-spec index)
            (index-unique-keys-p index))))

(defmethod index-similar-p ((index-1 index) (index-2 index))
  (and (index-spec-equal (index-spec index-1) (index-spec index-2))
       (equal (index-unique-keys-p index-1) (index-unique-keys-p index-2))))

;;
;; Trampolines
;;

(defmethod map-index ((index index) function
                      &rest args
                      &key min max include-min include-max
                      (equal nil)
                      (order :ascending))
  (declare (ignorable min max include-min include-max equal order))
  (apply #'map-index-data (index-data index) function args))

(defmethod index-insert ((index index) key value &key (if-exists :overwrite))
  (index-data-insert (index-data index) key value
                     :if-exists if-exists))

(defmethod index-delete ((index index) key value
                         &key (if-does-not-exist :ignore))
  (index-data-delete (index-data index) key value
                     :if-does-not-exist if-does-not-exist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Indexing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; NOTE: If you define your own indexing data structures, you need to supply
;; methods for the three generic functions below: MAP-INDEX-DATA,
;; INDEX-DATA-INSERT and INDEX-DATA-DELETE.

(defmethod map-index-data ((index btree) function
                           &rest args
                           &key min max include-min include-max
                           (equal nil equal-supplied)
                           (order :ascending))
  (declare (ignorable min max include-min include-max))
  (if equal-supplied
      (let ((value (btree-search index equal :errorp nil :default-value index)))
        (unless (p-eql value index)
          (if (btree-unique-keys-p index)
              ;; We have a single value: call FUNCTION directly.
              (funcall function equal value)
            ;; We have a persistent list of values: call FUNCTION for
            ;; each element of that list.
            (etypecase value
              ((or null persistent-cons)
               (p-mapc (lambda (elt) (funcall function equal elt))
                       value))
              (persistent-object-set
               (map-set-btree value
                              (lambda (elt) (funcall function equal elt))))))))
    (apply #'map-btree index function :order order args)))


(defmethod index-data-insert ((index btree) key value
                              &key (if-exists :overwrite))
  (btree-insert index key value :if-exists if-exists))

(defmethod index-data-delete ((index btree) key value
                              &key (if-does-not-exist :ignore))
  (btree-delete index key value :if-does-not-exist if-does-not-exist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Index specs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; An index spec is a symbol or a list starting with a symbol
;; and followed by a plist of keywords and values.
;; Examples: BTREE, (BTREE :KEY< <  :VALUE= P-EQL)

(defun make-index (index-spec unique-keys-p &key (class 'index))
  ;; NOTE: All index data classes must accept the :UNIQUE-KEYS-P initarg.
  (let ((data (if (symbolp index-spec)
                  (make-instance index-spec :unique-keys-p unique-keys-p)
                (apply #'make-instance
                       (first index-spec)
                       :unique-keys-p unique-keys-p
                       (rest index-spec)))))
    (make-instance class
                   :spec index-spec
                   :unique-keys-p unique-keys-p
                   :data data)))


(defun index-spec-equal (index-spec-1 index-spec-2)
  "Returns T iff two index specs are equal."
  (flet ((plist-subset-p (plist-1 plist-2)
           (loop for (key value) on plist-1 by #'cddr
                 always (equal (getf plist-2 key) value))))
    (or (eql index-spec-1 index-spec-2)
        (and (listp index-spec-1)
             (listp index-spec-2)
             (eql (first index-spec-1)
                  (first index-spec-2))
             (plist-subset-p (rest index-spec-1) (rest index-spec-2))
             (plist-subset-p (rest index-spec-2) (rest index-spec-1))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Defining index specs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(eval-when (:compile-toplevel :load-toplevel :execute)

  ;;
  ;; Defining index specs
  ;;

  (defparameter *index-specs*
    (make-hash-table))

  (defun define-index-spec (name spec &key (if-exists :overwrite))
    "NAME must be a keyword.  SPEC must be an index spec.  IF-EXISTS must be
either :OVERWRITE (default) or :ERROR."
    (assert (member if-exists '(:overwrite :error)))
    (when (eql if-exists :error)
      (let ((existing-spec (gethash name *index-specs*)))
        (when (and existing-spec
                   (not (index-spec-equal existing-spec spec)))
          (error "Index spec ~S is already defined.  Its definition is: ~S."
                 name existing-spec))))
    (setf (gethash name *index-specs*) spec))
  
  (defun find-index-spec (name &key (errorp t))
    (or (gethash name *index-specs*)
        (and errorp
             (error "Can't find index spec called ~S." name)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Predefined index specs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun trim-whitespace (string)
    (string-trim '(#\space #\tab #\return #\newline) string))

(eval-when (:compile-toplevel :load-toplevel :execute)

  (define-index-spec :number-index
                     '(btree :key< <
                             :value= p-eql
                             :value-type persistent-object))

  (define-index-spec :string-index
                     '(btree :key< string<
                             :value= p-eql
                             :value-type persistent-object
                             :key-type string))

  (define-index-spec :symbol-index
                     '(btree :key< string<
                             :value= p-eql
                             :value-type persistent-object
                             :key-type symbol))

  (define-index-spec :case-insensitive-string-index
                     '(btree :key< string-lessp
                             :value= p-eql
                             :value-type persistent-object
                             :key-type string))

  (define-index-spec :trimmed-string-index
                     ;; Like :STRING-INDEX, but with whitespace trimmed left
                     ;; and right.
                     '(btree :key< string<
                             :key-key trim-whitespace
                             :value= p-eql
                             :value-type persistent-object
                             :key-type string)))
