rucksack.pdf:
	pdflatex -shell-escape rucksack.tex
	makeindex rucksack.idx
	pdflatex -shell-escape rucksack.tex
	xdg-open rucksack.pdf

delete:
	rm -f rucksack.pdf

rebuild: delete rucksack.pdf
