

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;              
;;; MAYBE LATER: MERGING DEAD BLOCKS.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
;; The object is dead: try to merge neighbouring dead blocks
;; with this block.
(note-dead-object gc object-table object-id)
(loop (let ((next-block-position (+ position block-size)))
        (when (>= next-block-position heap-size)
          (return))
        (multiple-value-bind (next-block-size next-object-id)
            (read-block-start heap next-block-position)
          (if (object-alive-p object-table next-object-id)
              (return)
            (progn
              (note-dead-object gc object-table object-id)
              ;; Merge dead blocks.
              (incf block-size next-block-size))))))
           ;; Give the merged block back to the free-list manager.
           (deallocate-block heap position block-size)
           ;; Keep track of statistics.
           (incf (nr-dead-bytes gc) block-size))))

(defun note-dead-object (gc object-table object-id)
  ;; Keep track of statistics.
  (incf (nr-dead-objects gc))
  ;; Return the object-id to the object-table free list.
  (delete-object-id object-table object-id))
|#



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Copying garbage collector
;;;
;;; This code is incomplete.  At the moment we use a mark & sweep collector.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#| MAYBE LATER


(defclass copying-heap (garbage-collector serializer)
  ((space-0 :initarg :space-0 :reader space-0)
   (space-1 :initarg :space-1 :reader space-1)
   (from-space :accessor from-space)
   (to-space :accessor to-space)
   (evac-pointer :initform 0 :accessor evac-pointer
                 :documentation "The position in to-space where the next object
can be evacuated.")))

(defmethod collect-some-garbage ((heap copying-collector) amount)
  'DO-THIS)

(defmethod gc-work-for-size ((heap copying-collector) nr-allocated-octets)
  'DO-THIS)

(defmethod close-heap :after ((heap copying-heap))
  (close-heap (object-table heap)))

(defmethod deserialize-byte ((gc copying-collector)
                             &optional (eof-error-p t))
  ;; Hook into the serializer: deserializing a byte
  ;; means reading a byte from to-space at the trace-pointer.
  (file-position (to-space gc) (trace-pointer gc))
  (let ((result (read-byte (to-space gc) eof-error-p)))
    (incf (trace-pointer gc))
    result))



(defmethod initialize-instance :after ((gc copying-collector)
                                       &key &allow-other-keys)
  (setf (from-space gc) (space-0 gc)
        (to-space gc) (space-1 gc)))

(defmethod flip ((gc copying-collector))
  (rotatef (from-space gc) (to-space gc)))

(defun to-space-number (gc)
  (if (eql (to-space gc) (space-0 gc))
      0
    1))


;;
;; Evacuate
;;

(defgeneric evacuate (garbage-collector object-id object-table)
  (:documentation "Moves the specified object from from-space to to-space."))

(defmethod evacuate ((gc copying-collector) object-id object-table)
  (let ((heap-position (heap-position-for-id object-id object-table))
        (from-space (from-space gc))
        (to-space (to-space gc)))
    (file-position from-space heap-position)
    (let ((block-size (read-unsigned-bytes (cell-buffer gc) from-space)))
      ;; Read the block in from-space.
      (fill-heap-buffer gc from-space (- block-size +nr-block-header-bytes+))
      ;; And write it to to-space.
      (write-unsigned-bytes block-size (cell-buffer gc) to-space)
      (flush-heap-buffer gc (to-space gc) (evac-pointer to-space))
      ;; Update the object-table
      (setf (heap-position-for-id object-id object-table (to-space-number gc))
            (evac-pointer to-space))
      ;; Update evacuation pointer.
      (incf (evac-pointer to-space) block-size))))


;;
;; Trace
;; (The real work is done by trace-contents in serialize.lisp.)
;;


(defmethod scan-contents ((marker (eql +cached-object+))
                          buffer
                          (gc copying-collector))
  ;; Hook into the scanner: when the scanner finds a cached-object,
  ;; it evacuates that object and returns.
  (let ((object-id (deserialize gc)))
    (evacuate gc object-id (object-table (cache gc)))))

|#
